package helper

import "golang.org/x/crypto/bcrypt"

// Encrpyt function
func Encrpyt(data string) string {
	// Hash data with default cost as 10
	hash, _ := bcrypt.GenerateFromPassword([]byte(data), bcrypt.DefaultCost)

	// hash variable is byte, we need string
	return string(hash)
}

// Decrypt function
func Decrypt(hasedData string, data string) error {
	err := bcrypt.CompareHashAndPassword([]byte(hasedData), []byte(data))

	return err
}
