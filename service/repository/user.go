package repository

import (
	"database/sql"

	dm "gitlab.com/mangbinbin/services/user-service/service/domain"
)

// IUserRepository method
type IUserRepository interface {
	Store(user dm.User) error
	Fetch(offset int, limit int) ([]dm.User, error)
	GetByID(id string) (dm.User, error)
	GetByPhone(phone string) (dm.User, error)
	GetByKey(key string, value string) (dm.User, error)
	Update(user dm.User) error
	UpdateByKey(id string, key string, value string) error
}

// UserRepository is an object
type UserRepository struct {
	db *sql.DB
}

// NewUserRepository is an inital method for TrashRepository struct
// Called by Client (main.go)
func NewUserRepository(db *sql.DB) IUserRepository {
	return &UserRepository{
		db: db,
	}
}

// Store method
func (r *UserRepository) Store(user dm.User) error {
	query := `
		INSERT INTO users (id, user_firstname, user_lastname, user_email, user_photo, user_phone, user_passcode)
		VALUES ($1, $2, $3, $4, $5, $6, $7)
	`

	_, err := r.db.Query(
		query,
		user.ID,
		user.Firstname,
		user.Lastname,
		user.Email,
		user.Photo,
		user.Phone,
		user.Passcode,
	)

	return err
}

// Fetch method
func (r *UserRepository) Fetch(offset int, limit int) ([]dm.User, error) {

	query := `
		SELECT id, user_firstname, user_lastname, user_email, user_phone, user_photo, user_point, user_passcode, user_status
		FROM users OFFSET $1 LIMIT $2
	`

	rows, err := r.db.Query(query, offset, limit)

	if err != nil {
		return nil, err
	}

	users := make([]dm.User, 0)

	for rows.Next() {
		user := dm.User{}

		err = rows.Scan(
			&user.ID,
			&user.Firstname,
			&user.Lastname,
			&user.Email,
			&user.Phone,
			&user.Photo,
			&user.Point,
			&user.Passcode,
			&user.Status,
		)

		if err != nil {
			return nil, err
		}

		users = append(users, user)
	}

	return users, nil
}

// GetByID method
func (r *UserRepository) GetByID(id string) (dm.User, error) {
	query := `
		SELECT id, user_firstname, user_lastname, user_email, user_phone, user_photo, user_point, user_passcode, user_status
		FROM users
		WHERE id = $1
	`

	row := r.db.QueryRow(query, id)

	user := dm.User{}

	err := row.Scan(
		&user.ID,
		&user.Firstname,
		&user.Lastname,
		&user.Email,
		&user.Phone,
		&user.Photo,
		&user.Point,
		&user.Passcode,
		&user.Status,
	)

	if err != nil {
		return dm.User{}, err
	}

	return user, nil
}

// GetByPhone method
func (r *UserRepository) GetByPhone(id string) (dm.User, error) {
	query := `
		SELECT id, user_firstname, user_lastname, user_email, user_phone, user_photo, user_point, user_passcode, user_status
		FROM users
		WHERE user_phone = $1
	`

	row := r.db.QueryRow(query, id)

	user := dm.User{}

	err := row.Scan(
		&user.ID,
		&user.Firstname,
		&user.Lastname,
		&user.Email,
		&user.Phone,
		&user.Photo,
		&user.Point,
		&user.Passcode,
		&user.Status,
	)

	if err != nil {
		return dm.User{}, err
	}

	return user, nil
}

// GetByKey method
func (r *UserRepository) GetByKey(key string, value string) (dm.User, error) {
	query := `
		SELECT id, user_firstname, user_lastname, user_email, user_phone, user_photo, user_point, user_passcode, user_status
		FROM users
		WHERE ` + key + ` = $1
	`

	row := r.db.QueryRow(query, value)

	user := dm.User{}

	err := row.Scan(
		&user.ID,
		&user.Firstname,
		&user.Lastname,
		&user.Email,
		&user.Phone,
		&user.Photo,
		&user.Point,
		&user.Passcode,
		&user.Status,
	)

	if err != nil {
		return dm.User{}, err
	}

	return user, nil
}

// Update method
func (r *UserRepository) Update(user dm.User) error {
	query := `
		UPDATE users
		SET user_firstname=$2, user_lastname=$3, user_photo=$4 
		WHERE ID = $1`

	_, err := r.db.Query(query, user.ID, user.Firstname, user.Lastname, user.Photo)

	if err != nil {
		return err
	}

	return nil
}

// UpdateByKey method
func (r *UserRepository) UpdateByKey(id string, key string, value string) error {
	query := `UPDATE users SET ` + key + `=$2 WHERE ID = $1`

	_, err := r.db.Query(query, id, value)

	if err != nil {
		return err
	}

	return nil
}
