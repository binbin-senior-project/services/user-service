package repository

import (
	"database/sql"
)

// IAnalyticRepository method
type IAnalyticRepository interface {
	GetNewUserToday() (int, error)
}

// AnalyticRepository is an object
type AnalyticRepository struct {
	db *sql.DB
}

// NewAnalyticRepository is an inital method for TrashRepository struct
// Called by Client (main.go)
func NewAnalyticRepository(db *sql.DB) IAnalyticRepository {
	return &AnalyticRepository{
		db: db,
	}
}

// GetNewUserToday method
func (r *AnalyticRepository) GetNewUserToday() (int, error) {
	query := `SELECT COUNT(*) FROM users WHERE DATE("created_at") = current_date`

	row := r.db.QueryRow(query)

	var count int

	err := row.Scan(
		&count,
	)

	if err != nil {
		return 0, err
	}

	return count, nil
}
