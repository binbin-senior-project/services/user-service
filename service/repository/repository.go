package repository

// RegisterRepository struct
type RegisterRepository struct {
	Analytic    IAnalyticRepository
	User        IUserRepository
	Transaction ITransactionRepository
}
