package main

import (
	"github.com/joho/godotenv"

	db "gitlab.com/mangbinbin/services/user-service/service/database"
	amqp "gitlab.com/mangbinbin/services/user-service/service/delivery/amqp"
	grpc "gitlab.com/mangbinbin/services/user-service/service/delivery/grpc"
	r "gitlab.com/mangbinbin/services/user-service/service/repository"
	u "gitlab.com/mangbinbin/services/user-service/service/usecase"

	_ "github.com/lib/pq"
)

func main() {
	// For development purpose
	godotenv.Load()
	database := db.NewConnectDatabase()

	repo := r.RegisterRepository{
		Analytic:    r.NewAnalyticRepository(database),
		User:        r.NewUserRepository(database),
		Transaction: r.NewTransactionRepository(database),
	}

	// Register Broker Client
	ch := amqp.NewUserDelivery()
	// Register Subscriber broker
	amqp.NewAMQPSubscribe(repo, ch)
	// Register Publisher broker
	broker := amqp.NewAMQPPublish(ch)

	usecase := u.RegisterUsecase{
		Analytic:    u.NewAnalyticUsecase(repo, broker),
		User:        u.NewUserUsecase(repo, broker),
		Transaction: u.NewTransactionUsecase(repo, broker),
	}

	// Register GRPC Server
	grpc.NewUserDelivery(usecase)
}
