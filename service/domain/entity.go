package domain

import "time"

// User struct
type User struct {
	ID        string
	Firstname string
	Lastname  string
	Photo     string
	Phone     string
	Point     float64
	Email     string
	Passcode  string
	Status    string
}

// Transaction struct
type Transaction struct {
	ID          string
	UserID      string
	Type        string
	Point       float64
	Description string
	CreatedAt   time.Time
}
