package amqp

import (
	"log"

	amqp "github.com/streadway/amqp"
	dm "gitlab.com/mangbinbin/services/user-service/service/domain"
	h "gitlab.com/mangbinbin/services/user-service/service/helper"
)

// AMQPPublish Holayy!
type AMQPPublish struct {
	channel *amqp.Channel
}

// NewAMQPPublish constructor
func NewAMQPPublish(channel *amqp.Channel) *AMQPPublish {
	return &AMQPPublish{
		channel: channel,
	}
}

func (d *AMQPPublish) publish(exchange string, route string, payload []byte) {
	err := d.channel.Publish(
		exchange, // exchange
		route,    // routing key
		false,    // mandatory
		false,    // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        payload,
		})

	if err != nil {
		log.Fatal(err)
	}
}

// UserCreatedEvent method
func (d *AMQPPublish) UserCreatedEvent(user dm.UserCreatedEvent) {

	payload, err := h.SerializePayload(user)

	if err != nil {
		log.Fatal(err)
	}

	d.publish("user", "user.event.created", payload)
}

// UserUpdatedEvent method
func (d *AMQPPublish) UserUpdatedEvent(user dm.UserUpdatedEvent) {

	payload, err := h.SerializePayload(user)

	if err != nil {
		log.Fatal(err)
	}

	d.publish("user", "user.event.updated", payload)
}

// CreateNotification method
func (d *AMQPPublish) CreateNotification(notification dm.NotificationCreatedEvent) {
	payload, err := h.SerializePayload(notification)

	if err != nil {
		log.Fatal(err)
	}

	d.publish("notification", "notification.event.created", payload)
}
