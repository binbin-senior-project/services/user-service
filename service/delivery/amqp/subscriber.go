package amqp

import (
	"fmt"

	amqp "github.com/streadway/amqp"
	dm "gitlab.com/mangbinbin/services/user-service/service/domain"
	h "gitlab.com/mangbinbin/services/user-service/service/helper"
	r "gitlab.com/mangbinbin/services/user-service/service/repository"
)

// AMQPSubscribe struct
type AMQPSubscribe struct {
	repo    r.RegisterRepository
	channel *amqp.Channel
}

// NewAMQPSubscribe method
func NewAMQPSubscribe(repo r.RegisterRepository, channel *amqp.Channel) {
	subscriber := AMQPSubscribe{
		repo:    repo,
		channel: channel,
	}

	go subscriber.SubscribePointUpdateEvent()
	go subscriber.SubscribeTransactionCreatedEvent()
}

// SubscribePointUpdateEvent method
func (d *AMQPSubscribe) SubscribePointUpdateEvent() {
	msgs, err := d.channel.Consume(
		"update.point@user.queue", // queue
		"",                        // consumer
		true,                      // auto-ack
		false,                     // exclusive
		false,                     // no-local
		false,                     // no-wait
		nil,                       // args
	)

	failOnError(err, "Failed to register a SubscribePointUpdateEvent consumer")

	forever := make(chan bool)

	go func() {
		for msg := range msgs {
			point := dm.PointUpdatedEvent{}
			err = h.DeserializePayload(&point, msg.Body)

			d.repo.User.UpdateByKey(point.UserID, "user_point", fmt.Sprintf("%f", point.Point))
		}
	}()

	<-forever
}

// SubscribeTransactionCreatedEvent method
func (d *AMQPSubscribe) SubscribeTransactionCreatedEvent() {
	msgs, err := d.channel.Consume(
		"create.transaction@user.queue", // queue
		"",                              // consumer
		true,                            // auto-ack
		false,                           // exclusive
		false,                           // no-local
		false,                           // no-wait
		nil,                             // args
	)

	failOnError(err, "Failed to register a SubscribeNewMessagingEvent consumer")

	forever := make(chan bool)

	go func() {
		for msg := range msgs {
			trans := dm.UserTransactionCreatedEvent{}
			err = h.DeserializePayload(&trans, msg.Body)

			transaction := dm.Transaction{
				UserID:      trans.UserID,
				Type:        trans.Type,
				Point:       trans.Point,
				Description: trans.Description,
			}

			d.repo.Transaction.Store(transaction)
		}
	}()

	<-forever
}
