package grpc

import (
	"log"
	"net"
	"os"

	pb "gitlab.com/mangbinbin/services/user-service/service/delivery/grpc/proto"
	u "gitlab.com/mangbinbin/services/user-service/service/usecase"
	"google.golang.org/grpc"
)

// UserDelivery Holayy!
type UserDelivery struct {
	usecase u.RegisterUsecase
}

// NewUserDelivery method is an intial grpc package
// to start gRPC transportation
func NewUserDelivery(usecase u.RegisterUsecase) {
	// Register protobuf handler with UserDelievery struct
	delivery := &UserDelivery{
		usecase: usecase,
	}

	port := os.Getenv("SERVICE_PORT")

	// Listen out
	log.Println("Listen on port: " + port)

	// Create TCP Connection
	lis, err := net.Listen("tcp", ":"+port)

	// Detect connection error
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// Create GRPC service
	s := grpc.NewServer()

	// Register GRPC to protobuffer
	pb.RegisterUserServiceServer(s, delivery)

	// Register TCP connection to GRPC
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}
