package grpc

import (
	"context"

	pb "gitlab.com/mangbinbin/services/user-service/service/delivery/grpc/proto"
)

// GetNewUserToday method
func (d *UserDelivery) GetNewUserToday(ctx context.Context, req *pb.GetNewUserTodayRequest) (*pb.GetNewUserTodayReply, error) {
	count, err := d.usecase.Analytic.GetNewUserToday()

	if err != nil {
		return &pb.GetNewUserTodayReply{}, err
	}

	return &pb.GetNewUserTodayReply{
		Count: int32(count),
	}, nil
}
