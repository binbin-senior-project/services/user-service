package grpc

import (
	"context"

	pb "gitlab.com/mangbinbin/services/user-service/service/delivery/grpc/proto"
	dm "gitlab.com/mangbinbin/services/user-service/service/domain"
	h "gitlab.com/mangbinbin/services/user-service/service/helper"
)

// CreateUser method
func (d *UserDelivery) CreateUser(ctx context.Context, req *pb.CreateUserRequest) (*pb.CreateUserReply, error) {

	user := dm.User{
		ID:        req.Id,
		Firstname: req.Firstname,
		Lastname:  req.Lastname,
		Email:     req.Email,
		Photo:     req.Photo,
		Phone:     req.Phone,
		Passcode:  h.Encrpyt(req.Passcode),
	}

	// Create Account
	err := d.usecase.User.Store(user)

	if err != nil {
		return &pb.CreateUserReply{Success: false}, err
	}

	// No error in this case
	return &pb.CreateUserReply{Success: true}, nil
}

// GetUsers method
func (d *UserDelivery) GetUsers(ctx context.Context, req *pb.GetUsersRequest) (*pb.GetUsersReply, error) {
	// Create Account
	users, err := d.usecase.User.Fetch(int(req.Offset), int(req.Limit))

	var rspUsers []*pb.User

	if err != nil {
		return &pb.GetUsersReply{}, err
	}

	for _, user := range users {
		rspUsers = append(rspUsers, &pb.User{
			Id:        user.ID,
			Firstname: user.Firstname,
			Lastname:  user.Lastname,
			Email:     user.Email,
			Photo:     user.Photo,
			Phone:     user.Phone,
			Point:     user.Point,
			Status:    user.Status,
		})
	}

	// No error in this case
	return &pb.GetUsersReply{Users: rspUsers}, nil
}

// GetUser method
func (d *UserDelivery) GetUser(ctx context.Context, req *pb.GetUserRequest) (*pb.GetUserReply, error) {
	// Create Account
	user, err := d.usecase.User.GetByID(req.UserID)

	if err != nil {
		return &pb.GetUserReply{}, err
	}

	rsp := &pb.GetUserReply{
		User: &pb.User{
			Id:        user.ID,
			Firstname: user.Firstname,
			Lastname:  user.Lastname,
			Email:     user.Email,
			Photo:     user.Photo,
			Phone:     user.Phone,
			Point:     user.Point,
			Status:    user.Status,
		},
	}

	// No error in this case
	return rsp, nil
}

// GetUserByPhone method
func (d *UserDelivery) GetUserByPhone(ctx context.Context, req *pb.GetUserByPhoneRequest) (*pb.GetUserByPhoneReply, error) {
	// Create Account
	user, err := d.usecase.User.GetByPhone(req.Phone)

	if err != nil {
		return &pb.GetUserByPhoneReply{}, err
	}

	rsp := &pb.GetUserByPhoneReply{
		User: &pb.User{
			Id:        user.ID,
			Firstname: user.Firstname,
			Lastname:  user.Lastname,
			Photo:     user.Photo,
			Phone:     user.Phone,
			Point:     user.Point,
			Status:    user.Status,
		},
	}

	// No error in this case
	return rsp, nil
}

// UpdateUser method
func (d *UserDelivery) UpdateUser(ctx context.Context, req *pb.UpdateUserRequest) (*pb.UpdateUserReply, error) {

	user := dm.User{
		ID:        req.UserID,
		Firstname: req.Firstname,
		Lastname:  req.Lastname,
		Photo:     req.Photo,
	}

	err := d.usecase.User.Update(user)

	if err != nil {
		return &pb.UpdateUserReply{Success: false}, err
	}

	// No error in this case
	return &pb.UpdateUserReply{Success: true}, nil
}

// VerifyPasscode method
func (d *UserDelivery) VerifyPasscode(ctx context.Context, req *pb.VerifyPasscodeRequest) (*pb.VerifyPasscodeReply, error) {
	var (
		userID   = req.UserID
		passcode = req.Passcode
	)

	success, err := d.usecase.User.VerifyPasscode(userID, passcode)

	if err != nil {
		return &pb.VerifyPasscodeReply{Success: false}, err
	}

	return &pb.VerifyPasscodeReply{Success: success}, nil
}

// IsEmailExist method
func (d *UserDelivery) IsEmailExist(ctx context.Context, req *pb.IsEmailExistRequest) (*pb.IsEmailExistReply, error) {
	// Create Account
	success, _ := d.usecase.User.IsEmailExist(req.Email)

	return &pb.IsEmailExistReply{Success: success}, nil
}

// IsPhoneExist method
func (d *UserDelivery) IsPhoneExist(ctx context.Context, req *pb.IsPhoneExistRequest) (*pb.IsPhoneExistReply, error) {
	// Create Account
	success, _ := d.usecase.User.IsPhoneExist(req.Phone)

	return &pb.IsPhoneExistReply{Success: success}, nil
}

// ActivateUser method
func (d *UserDelivery) ActivateUser(ctx context.Context, req *pb.ActivateUserRequest) (*pb.ActivateUserReply, error) {
	// Create Account
	success, _ := d.usecase.User.Activate(req.UserID)

	return &pb.ActivateUserReply{Success: success}, nil
}

// BanUser method
func (d *UserDelivery) BanUser(ctx context.Context, req *pb.BanUserRequest) (*pb.BanUserReply, error) {
	// Create Account
	success, _ := d.usecase.User.Ban(req.UserID)

	return &pb.BanUserReply{Success: success}, nil
}
