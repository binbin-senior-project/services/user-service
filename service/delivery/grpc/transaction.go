package grpc

import (
	"context"

	pb "gitlab.com/mangbinbin/services/user-service/service/delivery/grpc/proto"
)

// GetUserTransactions method
func (d *UserDelivery) GetUserTransactions(ctx context.Context, req *pb.GetUserTransactionsRequest) (*pb.GetUserTransactionsReply, error) {
	transactions, err := d.usecase.Transaction.GetByUserID(req.UserID, int(req.Offset), int(req.Limit))

	var rspTrasctions []*pb.UserTransaction

	if err != nil {
		return &pb.GetUserTransactionsReply{}, err
	}

	for _, trans := range transactions {
		rspTrasctions = append(rspTrasctions, &pb.UserTransaction{
			Id:          trans.ID,
			Type:        trans.Type,
			Point:       trans.Point,
			Description: trans.Description,
			CreatedAt:   trans.CreatedAt.String(),
		})
	}

	return &pb.GetUserTransactionsReply{
		Transactions: rspTrasctions,
	}, nil
}

// GetUserTransaction method
func (d *UserDelivery) GetUserTransaction(ctx context.Context, req *pb.GetUserTransactionRequest) (*pb.GetUserTransactionReply, error) {
	trans, err := d.usecase.Transaction.GetByID(req.TransactionID)

	if err != nil {
		return &pb.GetUserTransactionReply{}, err
	}

	rspTransaction := &pb.UserTransaction{
		Id:          trans.ID,
		Type:        trans.Type,
		Point:       trans.Point,
		Description: trans.Description,
		CreatedAt:   trans.CreatedAt.String(),
	}

	return &pb.GetUserTransactionReply{
		Transaction: rspTransaction,
	}, nil
}
