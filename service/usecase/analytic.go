package usecase

import (
	amqp "gitlab.com/mangbinbin/services/user-service/service/delivery/amqp"
	r "gitlab.com/mangbinbin/services/user-service/service/repository"
)

// IAnalyticUsecase method
type IAnalyticUsecase interface {
	GetNewUserToday() (int, error)
}

// AnalyticUsecase struct
type AnalyticUsecase struct {
	repo   r.RegisterRepository
	broker *amqp.AMQPPublish
}

// NewAnalyticUsecase is an inital package method
func NewAnalyticUsecase(repo r.RegisterRepository, broker *amqp.AMQPPublish) IAnalyticUsecase {
	return &AnalyticUsecase{
		repo:   repo,
		broker: broker,
	}
}

// GetNewUserToday method
func (u *AnalyticUsecase) GetNewUserToday() (int, error) {
	count, err := u.repo.Analytic.GetNewUserToday()

	if err != nil {
		return count, err
	}

	return count, nil
}
