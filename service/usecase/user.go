package usecase

import (
	amqp "gitlab.com/mangbinbin/services/user-service/service/delivery/amqp"
	dm "gitlab.com/mangbinbin/services/user-service/service/domain"
	h "gitlab.com/mangbinbin/services/user-service/service/helper"
	r "gitlab.com/mangbinbin/services/user-service/service/repository"
)

// IUserUsecase method
type IUserUsecase interface {
	Store(user dm.User) error
	Fetch(offset int, limit int) ([]dm.User, error)
	GetByID(id string) (dm.User, error)
	GetByPhone(phone string) (dm.User, error)
	Update(user dm.User) error
	VerifyPasscode(id string, passcode string) (bool, error)
	IsEmailExist(email string) (bool, error)
	IsPhoneExist(phone string) (bool, error)
	Activate(id string) (bool, error)
	Ban(id string) (bool, error)
}

// UserUsecase struct
type UserUsecase struct {
	repo   r.RegisterRepository
	broker *amqp.AMQPPublish
}

// NewUserUsecase is an inital package method
// called by Client (main.go)
func NewUserUsecase(repo r.RegisterRepository, broker *amqp.AMQPPublish) IUserUsecase {
	return &UserUsecase{
		repo:   repo,
		broker: broker,
	}
}

// Store method
func (u *UserUsecase) Store(user dm.User) error {
	// Create Account and retrive User
	err := u.repo.User.Store(user)

	if err != nil {
		return err
	}

	go u.broker.UserCreatedEvent(dm.UserCreatedEvent{
		UserID:    user.ID,
		Firstname: user.Firstname,
		Lastname:  user.Lastname,
		Phone:     user.Phone,
	})

	return nil
}

// Fetch method
func (u *UserUsecase) Fetch(offset int, limit int) ([]dm.User, error) {
	// Create Account and retrive User
	users, err := u.repo.User.Fetch(offset, limit)

	if err != nil {
		return []dm.User{}, err
	}

	return users, nil
}

// GetByID method
func (u *UserUsecase) GetByID(id string) (dm.User, error) {
	// Create Account and retrive User
	user, err := u.repo.User.GetByID(id)

	if err != nil {
		return dm.User{}, err
	}

	return user, nil
}

// GetByPhone method
func (u *UserUsecase) GetByPhone(phone string) (dm.User, error) {
	// Create Account and retrive User
	user, err := u.repo.User.GetByPhone(phone)

	if err != nil {
		return dm.User{}, err
	}

	return user, nil
}

// Update method
func (u *UserUsecase) Update(user dm.User) error {
	err := u.repo.User.Update(user)

	if err != nil {
		return err
	}

	go u.broker.UserCreatedEvent(dm.UserCreatedEvent{
		UserID:    user.ID,
		Firstname: user.Firstname,
		Lastname:  user.Lastname,
		Phone:     user.Phone,
	})

	return nil
}

// VerifyPasscode method
func (u *UserUsecase) VerifyPasscode(userID string, passcode string) (bool, error) {
	user, err := u.repo.User.GetByID(userID)

	// Verify passcode
	err = h.Decrypt(user.Passcode, passcode)

	if err != nil {
		return false, err
	}

	return true, nil
}

// IsPhoneExist method
func (u *UserUsecase) IsPhoneExist(phone string) (bool, error) {
	_, err := u.repo.User.GetByKey("user_phone", phone)

	if err != nil {
		return false, err
	}

	return true, nil
}

// IsEmailExist method
func (u *UserUsecase) IsEmailExist(email string) (bool, error) {
	_, err := u.repo.User.GetByKey("user_email", email)

	if err != nil {
		return false, err
	}

	return true, nil
}

// Activate method
func (u *UserUsecase) Activate(id string) (bool, error) {

	_, err := u.repo.User.GetByID(id)

	if err != nil {
		return false, err
	}

	err = u.repo.User.UpdateByKey(id, "user_status", "active")

	if err != nil {
		return false, err
	}

	go u.broker.CreateNotification(dm.NotificationCreatedEvent{
		UserID: id,
		Title:  "แจ้งเตือนบัญชี",
		Body:   "บัญชีของคุณได้รับการอนุมัติการใช้งาน",
	})

	return true, nil
}

// Ban method
func (u *UserUsecase) Ban(id string) (bool, error) {
	_, err := u.repo.User.GetByID(id)

	if err != nil {
		return false, err
	}

	err = u.repo.User.UpdateByKey(id, "user_status", "banned")

	if err != nil {
		return false, err
	}

	go u.broker.CreateNotification(dm.NotificationCreatedEvent{
		UserID: id,
		Title:  "แจ้งเตือนบัญชี",
		Body:   "บัญชีของคุณโดนระงับการใช้งาน",
	})

	return true, nil
}
