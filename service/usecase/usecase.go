package usecase

// RegisterUsecase struct
type RegisterUsecase struct {
	Analytic    IAnalyticUsecase
	User        IUserUsecase
	Transaction ITransactionUsecase
}
