module gitlab.com/mangbinbin/services/user-service/service

go 1.13

require (
	github.com/golang/protobuf v1.4.0
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.4.0
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
	google.golang.org/grpc v1.29.1
)
