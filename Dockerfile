# Build Stage
FROM golang:latest AS builder

WORKDIR /app

COPY /service/go.mod /service/go.sum ./

RUN go mod download

COPY /service .

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o service .

# Production Stage
FROM alpine:latest

RUN apk --no-cache add ca-certificates

COPY --from=builder ./app/service . 

RUN chmod +x ./service

CMD ["./service"]

