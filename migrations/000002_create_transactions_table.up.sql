CREATE TYPE transaction_type AS ENUM ('collect', 'redeem', 'transfer');

CREATE TABLE IF NOT EXISTS transactions(
  id serial NOT NULL PRIMARY KEY,
  user_id varchar(36) NOT NULL,
  transaction_type TRANSACTION_TYPE NOT NULL,
  transaction_point REAL NOT NULL,
  transaction_description VARCHAR(255) NOT NULL,
  transaction_created_at TIMESTAMP default CURRENT_TIMESTAMP NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id)
);
