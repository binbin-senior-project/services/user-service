CREATE TYPE user_status AS ENUM ('active', 'banned');

CREATE TABLE IF NOT EXISTS users(
  id varchar(36) PRIMARY KEY,
  user_firstname VARCHAR (255),
  user_lastname VARCHAR (255),
  user_photo VARCHAR (255),
  user_phone VARCHAR (255) UNIQUE,
  user_point REAL DEFAULT 0,
  user_email VARCHAR (255) NOT NULL,
  user_passcode VARCHAR (255) NOT NULL,
  user_status USER_STATUS DEFAULT 'active',
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
